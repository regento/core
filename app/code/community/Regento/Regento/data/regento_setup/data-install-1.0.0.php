<?php
/**
 *
 * Regento
 *
 * PHP Version 5.3
 *
 * @category Regento
 * @package Regento_Regento
 * @copyright Copyright (c) 2014 Valerio Masciotta
 * @license
 * @link http://www.regento.it
 *
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$connection = $this->getConnection();

$installer->startSetup();



for ($i = 1; $i <= 10; ++$i) {
    $is_active = ($i >= 3)? 0 : 1;
    $connection->insert(
        $installer->getTable('cms/block'), array(
                                                'title'         => "Regento Bottom " . $i,
                                                'identifier'    => "regento_bottom_static_block_" . $i,
                                                'content'       => "<h1>Bottom block " . $i
                                                . "</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper congue mi. Nulla facilisi. Integer sodales molestie leo in ullamcorper. Praesent sit amet dapibus ligula. Proin ullamcorper vestibulum scelerisque. Integer quis massa erat. Phasellus et nisl non felis faucibus laoreet.</p><div><ul class=\"disc\"><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li></ul></div>",
                                                'creation_time' => now(),
                                                'update_time'   => now(),
                                                'is_active'     => $is_active,
                                           )
    );

    $connection->insert(
        $installer->getTable('cms/block_store'), array(
                                                      'block_id' => $connection->lastInsertId(),
                                                      'store_id' => 0
                                                 )
    );
}
$installer->endSetup();