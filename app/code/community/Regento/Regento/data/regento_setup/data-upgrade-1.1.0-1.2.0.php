<?php
/**
 * @ $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$connection = $this->getConnection();

$installer->startSetup();

    $connection->insert(
        $installer->getTable('cms/block'), array(
                                                'title'         => "Regento Contact Page ",
                                                'identifier'    => "regento_contacts_page",
                                                'content'       => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper congue mi. Nulla facilisi. Integer sodales molestie leo in ullamcorper. Praesent sit amet dapibus ligula. Proin ullamcorper vestibulum scelerisque. Integer quis massa erat. Phasellus et nisl non felis faucibus laoreet.</p>
                                                                    <div><ul class=\"disc\"><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li></ul></div>",
                                                'creation_time' => now(),
                                                'update_time'   => now(),
                                                'is_active'     => 1,
                                           )
    );

    $connection->insert(
        $installer->getTable('cms/block_store'), array(
                                                      'block_id' => $connection->lastInsertId(),
                                                      'store_id' => 0
                                                 )
    );

$installer->endSetup();