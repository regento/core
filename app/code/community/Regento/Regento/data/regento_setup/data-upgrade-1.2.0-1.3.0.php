<?php
/**
 * @ $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$connection = $this->getConnection();

$installer->startSetup();
for($i=1; $i<=3; ++$i){
    $connection->insert(
        $installer->getTable('cms/block'), array(
            'title'         => "Regento Promotions Top ".$i,
            'identifier'    => "regento_promotions_top_".$i,
            'content'       => "Promotions text ".$i,
            'creation_time' => now(),
            'update_time'   => now(),
            'is_active'     => 1,
        )
    );

    $connection->insert(
        $installer->getTable('cms/block_store'), array(
            'block_id' => $connection->lastInsertId(),
            'store_id' => 0
        )
    );
}

$connection->insert(
    $installer->getTable('cms/block'), array(
        'title'         => "Regento Sidebar Block Right ",
        'identifier'    => "regento_sidebar_block_right",
        'content'       => "Content ",
        'creation_time' => now(),
        'update_time'   => now(),
        'is_active'     => 0,
    )
);

$connection->insert(
    $installer->getTable('cms/block_store'), array(
        'block_id' => $connection->lastInsertId(),
        'store_id' => 0
    )
);

$connection->insert(
    $installer->getTable('cms/block'), array(
        'title'         => "Regento Sidebar Block Left ",
        'identifier'    => "regento_sidebar_block_left",
        'content'       => "Content ",
        'creation_time' => now(),
        'update_time'   => now(),
        'is_active'     => 0,
    )
);

$connection->insert(
    $installer->getTable('cms/block_store'), array(
        'block_id' => $connection->lastInsertId(),
        'store_id' => 0
    )
);
$installer->endSetup();