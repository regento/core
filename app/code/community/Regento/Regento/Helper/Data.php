<?php
/**
 *
 * Regento
 *
 * PHP Version 5.3
 *
 * @category Regento
 * @package Regento_Goodrelations
 * @copyright Copyright (c) 2014 Valerio Masciotta
 * @license
 * @link http://www.regento.it
 *
 */

/**
 * Regento helper
 */
class Regento_Regento_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * This method returns the current Regento Version
     * @return string version number
     */
    public function getVersion(){
        $version = Mage::getConfig()->getModuleConfig('Regento_Regento')->version;
        return $version;
    }
}
