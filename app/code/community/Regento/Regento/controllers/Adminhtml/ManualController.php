<?php

/**
 * Regento Manual Controller
 */
class Regento_Regento_Adminhtml_ManualController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('regento/modalbox');
        $this->loadLayout();
        $block = $this->getLayout()->createBlock('regento/adminhtml_manual','regento_manual');
        $this->_addContent($block);
        $this->renderLayout();
    }
}