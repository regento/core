<?php
/**
 *
 *
 * Regento
 *
 * PHP Version 5.3
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Regento
 * @package   Regento_Regento
 * @copyright Copyright (c) 2014 Valerio Masciotta
 * @license
 */

/**
 * PushJs Block
 *
 * This class is a clone of Html_Head.
 *
 * It adds javascripts and css outside the page's head, at the very bottom of the page
 *
 *
 * @file Pushjs.php
 * @author Valerio Masciotta <sviluppo@valeriomasciotta.it>
 *
 */
class Regento_Regento_Block_Pushjs extends Mage_Page_Block_Html_Head
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('page/html/regento_elements/pushjs.phtml');
    }
}