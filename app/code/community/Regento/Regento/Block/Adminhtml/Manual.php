<?php

/**
 * Regento Manual Block
 */
class Regento_Regento_Block_Adminhtml_Manual extends Mage_Core_Block_Template
{
    const REGENTO_MANUAL_URL = 'http://www.valeriomasciotta.it/regento-manual';

    protected function _toHtml()
    {
        $output = $this->_getStyles();
        $output .= "<iframe id=\"regento-manual\" scroll=\"auto\" src=\"{$this->_getIframeUrl()}\" style=\"width: 100%; height: 100%; \"></iframe>";
        return $output;
    }

    protected function _getIframeUrl()
    {
        $iframeUrl = self::REGENTO_MANUAL_URL;
        return $iframeUrl;
    }

    protected function _getStyles()
    {
        $styles = '
<style type="text/css" media="screen">
body, html {width: 100%;height: 100%;overflow: hidden;}
.middle {padding:10px; }
iframe {display:block; width:100%;height:100%; min-height:440px !important;border-bottom:0px; border:none;}
.footer, #loading-mask {display:none;}
</style>
';
        return $styles;
    }
}