/**
 * Regento 1.0.0
 */

jQuery.fn.exists = function () {
    return this.length > 0;
}
;(function($){
    if($('#scroll-to-top').exists()){
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('#scroll-to-top').fadeIn();
            } else {
                $('#scroll-to-top').fadeOut();
            }
        });
        $('#scroll-to-top').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
    }
})(jQuery);