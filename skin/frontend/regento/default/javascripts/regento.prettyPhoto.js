;(function ($, window, undefined) {
    if ($("a[rel^='prettyPhoto']").exists()) {
        $("a[rel^='prettyPhoto']").prettyPhoto({
            social_tools: false
        });
    }
})(jQuery);