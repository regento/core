//random
if(jQuery('#randomProductsList').size() !== 0){
    jQuery('#randomProductsList').carouFredSel({
        responsive: true,
        infinite:true,
        auto: false,
        width: "98%",
        height:'auto',
        prev : "#carousel_random_prev",
        next : "#carousel_random_next",
        items: {
            width: 200,
            //height: '30%', // optionally resize item-height
            visible: {
                min: 2,
                max: 5
            }
        },
        scroll: {
            items: 1,
            pauseOnHover: true
        }             
    });        
}


//mostviewed
if(jQuery('#mostviewed').size() !== 0){
    jQuery('#mostviewed').carouFredSel({
        responsive: true,
        infinite:true,
        auto: false,
        width: "98%",
        height:'auto',
        prev : "#carousel_mw_prev",
        next : "#carousel_mw_next",
        items: {
            width: 200,
            //height: '30%', // optionally resize item-height
            visible: {
                min: 2,
                max: 5
            }
        },
        scroll: {
            items: 1,
            pauseOnHover: true
        }             
    });        
}

//bestseller
if(jQuery('#bestseller').size() !== 0){
    jQuery('#bestseller').carouFredSel({
        responsive: true,
        infinite:true,
        auto: false,
        width: "98%",
        height:'auto',
        prev : "#carousel_bs_prev",
        next : "#carousel_bs_next",
        items: {
            width: 200,
            //height: '30%', // optionally resize item-height
            visible: {
                min: 2,
                max: 5
            }
        },
        scroll: {
            items: 1,
            pauseOnHover: true
        }             
    });        
}